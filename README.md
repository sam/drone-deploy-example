# Deploy Example

This is an example of a simple deployment of a web application using the [Deno Deploy](https://git.froth.zone/actions/deno-deploy) module.

It is deployed in two places:

- GitHub Actions: <https://github-actions-deploy.deno.dev> ([Action Link](./.github/workflows/deploy.yaml))
- Drone CI: <https://drone-deploy.deno.dev> ([Drone Link](./.drone.yml))
